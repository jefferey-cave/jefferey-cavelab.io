---
layout: default
---

Born in Calgary, Alberta, I discovered a passion for business data 
analysis in my teens. This passion blossomed into a career as a professional 
software developer and business analyst. 

More than just developing software, I love to share the hidden beauty of 
data with young developers and clients alike; and appreciate those who love 
to share their art [regardless of what that art is](./scholarship/).

Having tried my hand at recreational beekeeping and firefighting, I can 
often be found gathering samples of Halifax fish populations in his spare time 
(aka: fishing).


