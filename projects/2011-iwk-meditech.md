---
layout: default
title: IWK Meditech Upgrade
---

2010-2011

IWK's Meditech E-Charting system was reaching its end of life, and an upgrade was required to continue to receive support. Affecting all aspects of patient care, a successful deployment, and therefore testing, was critical to the release. By gathering a listing of tests as recommended by Meditech, and procedures as used by IWK, I developed standardized testing documentation, each test containing clearly defined objectives, steps-by-step instructions, and success/failure definitions; suitable for use by non-expert staff. Building on the detailed documentation generated, I developed, demonstrated, and used an automated tester, which should act as a prototype for future testing. The new version of Meditech was successfully deployed on Jan 5, 2011.

Results

* identified 2 new life-safety issues
* performed all testing 2 weeks ahead of schedule
* created standardized, detailed, test documentation

Bonus

* created an automated tester
* wrote prototype automated tests for each testing department for large test-data sets
* automated tool for gathering test data from in-house and vendor and generation of testing assignments

Meditech, C#, MS Access, NUnit, Boston Workstation, Documentation, Process Development, VBScript



