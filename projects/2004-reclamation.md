---
layout: default
title: Remediation & Reclamations

---

```
Company.: Recon8er
Date....: 2001-2004
```

Cleaning and erasing the signs of an oil or gas well (cleaning polluted soil, 
removing roads, planting native flora) was the task taken on by Recon8er. 
Coordinating the specialized fields, and resources, across the long periods of 
time it takes to repair the land required a complex system. Having developed 
an in-house MS Access system to track the progress and billing, Recon8er was 
experiencing the classic problem of systems outgrowing their non-technical 
developers. Sunergon took on the task of managing the maintenance of their 
system. Within this  ongoing maintenance I was tasked with bug repair, data 
normalization, and feature enhancement. Recon8er was able to easily track 
ongoing projects spanning decades and hundreds of specialists.

Results

* data normalization removed duplicate data errors
* weekly delivery of enhancements
* system speed sufficiently increased to allow contractors direct access

<sub>MS Access, VBScript, Backup Scheduling, Customer Support</sub>

