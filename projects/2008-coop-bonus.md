---
layout: default
title: Calgary Co-op Bonus and Incentives
---

2007-2008

Introducing a new incentive structure introduced new requirements for the 
Compensation Manager. Based on corporate, departmental, and personal 
performance, calculation of the bonus was both complex and confidential. This 
required that the Compensation Manager performed the calculation, by hand, for 
approximately 4500 employees. In order to reduce the labour requirement of the 
Compensation Manager, I documented the process in detail, proposed a system 
for reporting, and integration with the payroll system. In order to balance 
the needs of the Compensation Manager for immediate delivery, and the IT 
department's need to plan and implement, I developed a set of spreadsheets to 
act as an immediate aid to calculation while the IT department worked on a 
more permanent solution for the following year using the spreadsheets as a 
guide to implementation.

Results

* opportunity identified and recommendations approved
* IT and Business needs successfully balanced

Bonus

* temporary solution implemented to gain immediate results and relieve interdepartmental tension

<sub>Documentation, Negotiation, Excel, Communication </sub>




