---
layout: default
title: Adobe CQ Mobile Website
---

[http://dal.ca/agriculture](http://dal.ca/agriculture)
2013

With hand-held devices forming a larger portion of internet traffic, it was 
decided that changing the Dalhousie University website to be responsive to 
smaller form factors was required. The Faculty of Agriculture website, being 
the newest of Dalhousie's sites was chosen to undergo the initial changes. 

While the Faculty of Agriculture was the sub-site selected for visible changes, 
a major overhaul of the entire site infrastructure was undertaken to make it 
mobile ready. Given the extensive changes to the underlying infrastructure, 
early detection of errors, especially in unrelated systems (eg. Medicine, Law, 
etc.) was highly critical.

Results

* Stakeholders engaged in establishing their areas of expertise
* formalized Pass/Fail process implemented (test definitions prior to 
  development)

Bonus

* need for Test Driven Development processes identified
* TDD processes outlined and proposed as part of ongoing development and 
  regression testing

<sub>Test Driven Development, Agile (Kanban/Scrum), TestNG, UnitTesting, Automated Regression Testing, TestNG, Selenium, SVN/Subversion, Adobe CQ, Java, JUnit</sub>

