---
layout: default
title: Previewdata
---

[https://www.previewdata.com](https://www.previewdata.com)

2002-2007

As an independent auditor, Reeves' was tasked as a go-between for drilling 
companies, governments, and investors. Their challenge was to collect data 
from wells in some of the most remote places on earth, and securely deliver 
assessments to authorized parties. Due to political instability, poor 
transport infrastructure, and the high value of the data involved, Reeves 
could no longer rely on their existing courier based delivery mechanisms.

In order to overcome these obstacles, I worked directly with the client to 
develop a central clearing house for their data. Data collected in the field 
was securely transported via Satellite, modem, or internet, to a central 
system that automated report generation; reports were then faxed directly to 
client offices. As internet usage became more prevalent, fax delivery was 
phased out in favour of email and web delivery. 

Development of this system, dramatically set Reeves apart from their 
competitors. Originally known as Reeview and developed for Reeves 
International, this system was the impetus for Precision Drilling to acquire 
Reeves (renaming the system to Preview), followed an acquisition by 
Weatherford International. After 6 years of cultivating this system, I worked 
closely with developers in Australia and England, to transition control to 
Weatherford's in-house systems department.

Results

* reduced delivery time from weeks to minutes
* eliminated data interception
* system logs used successfully in two internal theft investigations

Bonus

* corporate acquisition of Reeves & Precision Dept
* project was foundation for Sunergon's growth
* awarded “The Queen's Awards for Enterprise Innovation” 2010

<sub>MS SqlServer, Sqlite, SVN, Issue Tracking, Process Management, Visual Studio, Linux, Cygwin, Bash Scripts, Web Applications, Client/Server, VBScript, VB.NET, Mono, C#, ASP, ASP.NET, SAAS, AJAX, JavaScript, HTML, XML, JSON, Transact SQL</sub>

