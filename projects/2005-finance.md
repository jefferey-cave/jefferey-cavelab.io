---
layout: default
title: Fund Management System
---

```
Company.: Savoy Capital
Date....: 2005 & 2007
```

Savoy was a small Hedge Fund, who had outgrown their existing record keeping 
system (Excel Spreadsheets), leading to difficulty managing the volume of 
data, and causing large accounting errors. Unfortunately, existing solutions to 
the problem were priced outside of a small company's reach. As the senior 
developer on the project, I worked with my team to design and develop a custom 
system for Savoy that was affordable, accurate, and automated; leading to 
further demand from other Asset Management Companies, for a similar product. 

Results

* automated reporting
* eliminated overtime requirement for data management personnel
* accounting error reduced from dollars to +/-0.001$

Bonus

* traders able to access data and reporting directly
* identified historic accounting errors
* became a regular product for Sunergon, filling the small/mid-sized market

<sub>MS Access, VB Script, C#, MS SqlServer, PostgreSQL</sub>

