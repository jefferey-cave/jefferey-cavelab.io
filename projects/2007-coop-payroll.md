---
layout: default
title: Payroll Processing and Integration
---

2007

Every week, an automated system collects all electronic time-cards, and enters 
the data into the payroll system. Running on Sunday afternoons, the system 
failed frequently requiring manual intervention. Combined with an extended 
processing time, an undue burden was being placed on the Payroll Manager, and 
IT support. Over the course of 6 months, I took the initiative to identify and 
revise the long running process. By reducing the processing time, more time 
was created to deal with problems, reducing stress and overtime. Incidentally, 
the error was identified and corrected during this process.

Results

* processing time reduced from 8h to 2ms
* eliminated overtime burden

Bonus

* added an email notification of success/failure for payroll manager

<sub>Oracle ERP, PL/SQL, Java, SQL, Subversion (SVN), Kronos Workforce, VBScript, MS Batch files, MS Access, Documentation, Jira Issue and Project Tracking, Analysis</sub>



