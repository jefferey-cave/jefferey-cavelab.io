---
layout: default
title: RWDI – Virtual Wind
---


[http://www.rwdi.com/state_of_the_art/virtualwind](http://www.rwdi.com/state_of_the_art/virtualwind)

2005

As a large engineering firm, RWDI was seeking a way to deliver its wind models 
to clients. The new Virtual Wind department was seeking a way to bridge a web 
presence with its highly technical engineering models. Working with a 
marketing graphical designer, and an RWDI engineer, I developed a web 
application that allowed for content management by managers, graphic design 
for attracting new customers, and the delivery of virtual wind tunnel 
simulations to subscription based clients.

Results

* database integration with simulation software
* negotiated needs of technical and marketing groups

<sub>ASP, VBScript, MS SqlServer, JavaScript</sub>

