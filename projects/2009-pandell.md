---
layout: default
title: Pandell Competitor Conversion
---

2009

Customer's transitioning from their existing platform to 
[Pandell's JVNexus](https://www.pandell.com/oilandgas/pandelljv/) 
platform, required technical and accounting assistance to ensure continuing 
data integrity and business operations. While performing conversions through 
manipulation of SQL queries, I documented and develop a process management 
system for coordinating the efforts of accountants and techs, as well as 
automating several of the more intensive data manipulations, and testing the 
data integrity itself.

Results

* automated “payment reconciliation” reducing labour from 2 weeks to 15 minutes
* reconstructed automated “account balancing” removing significant error 
  (correction time reduced from 256 man hours to 6 processing hours)

Bonus

* documented suggested best practices for maintaining and measuring performance
* significant Agent of Change encouraging metrics of project to be maintained

<sub>MS Access, MS SqlServer, Oracle, Foxpro, C#, Delphi, VB Script, NUnit, Perforce, Subversion (SVN)</sub>


