---
layout: default
title: Sunergon Product Management
---

2004-2007

As a small company, Sunergon had few requirements for a formal development 
process. As it grew, projects became more complex and changed hands between 
developers. A formal process for managing the planning, production and 
maintenance of projects became necessary. I developed several processes, and 
implemented key technologies, to improve quality and efficiency.

Results

* Revision Management to isolate developers workspace
* Unit Testing to ensure efficient regression testing
* Agile techniques to focus management and developers

<sub>Subversion, Nunit, Issue Tracking, Continuous Integration</sub>

