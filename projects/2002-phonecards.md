---
layout: default
title: Pre-paid Phone Cards
---

```
Company.: Telus
Date....: 2002
```

As part of Sunergon's ongoing development for Telus Communication's cellular 
billing system, Telus required a reliable and fast means of producing unique 
pre-paid card numbers. While the system was required, it needed to silently 
fit into the existing system infrastructure.

Results

* Validated unique against central system
* Seamless integration with existing systems

Bonus

* Implementation of Luhn check-sums for more rapid validation of number entry 
  in future systems

<sub>MS Access, VB6, MS SqlServer</sub>

