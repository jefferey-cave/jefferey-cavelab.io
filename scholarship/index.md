---
layout: default
title: Sharon & Jeff Cave Scholarship
---

# Philosophy

Regardless of the form, art requires effort, sacrifice, and study from the individual. However, it is only through the challenge of cooperation and shared ideas that we can attain mastery.

 

# Purpose

The Sharon & Jeff Cave Scholarship for the Performing Arts, recognizes
students that have demonstrated commitment and initiative to develop 
themselves as individuals, as well as showing generosity in sharing their 
experience with peers and the dance community.

# Guidelines

The scholarship consists of both a tuition portion and direct prize portion 
awarded to a single student. The tuition portion shall be released to the 
school on behalf of the student at the beginning of the term. The prize portion 
shall be released directly to the student at convocation.

Recipients must be willing to have their name, picture, and short biography 
published.

# Amount

One years tuition for a weekly, 1.5 hour, ballet class at the [Maritime 
Conservatory](http://maritimeconservatory.com/); as well as a $150 cash prize.

# Recipients

| 2018 | [](2018-iana) |
| 2017 | [Mabel MacNeil](2017-mabel) |
