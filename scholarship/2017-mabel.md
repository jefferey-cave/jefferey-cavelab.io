---
layout: default
title: Scholarship 2017 - Mabel MacNeil
---

Mabel MacNeil just graduated high school in the 2016/2017 year with honours.

Mabel has been a dancer at the Maritime Conservatory of Performing Arts (MCPA) for 12 years, and has been accepted into the Teacher Training Program for the 2017/2018 year. She has achieved her RAD intermediate ballet exam certificate, and has received the Senior Dance certificate. Mabel has also assisted dance classes and summer dance camps for 4 years. She was a member of Girl Guides and has received her Lady Baden-Powell Award.

In her free time Mabel enjoys all forms of creativity.

