Quite often companies want to know about me, but I have a hard time gathering 
all the information together for them to find. This site is a portfolio that 
contains all the information I can think someone may find useful. Partially for 
my reference, partially for others.