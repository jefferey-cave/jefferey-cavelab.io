---
layout: default
title: Profiles
---

There are of course the standard places

* [<i class="fab fa-gitlab"></i> GitLab](https://gitlab.com/jefferey-cave)
* [<i class="fab fa-github"></i> GitHub](https://github.com/JeffCave)
* [<i class="fab fa-linkedin-in"></i> LinkedIn](https://www.linkedin.com/in/jeffereycave/)
* [<i class="fab fa-stack-overflow"></i> StackOverflow](https://stackoverflow.com/users/story/1961413)

# Classics

Over the years of the Web, I have left a trail of references around. Some of 
these I still refer to.

* [&Tau; Tek-Tips](http://www.tek-tips.com/userinfo.cfm?member=kavius)
* [<i class="fab fa-linux"></i> Linux Questions](https://www.linuxquestions.org/questions/user/kavius-24015/)




<!--
<div class="github-card" data-github="JeffCave" data-width="400" data-height="" data-theme="default"></div>
<script src="//cdn.jsdelivr.net/github-cards/latest/widget.js"></script>
-->
