---
layout: default
title: Pandell
---

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Corporation",
  "name": "Pandell Technology",
  "url": "https://www.pandell.com/",
  "numberOfEmployees":{
    "value":50
  },
  "logo":"https://www.pandell.com/img/logos/pandell-nav-med.png",
	"location":{
		"@type":"Place",
		"name":"Calgary Co-op Head Office",
		"geo":{
			"@type": "GeoCoordinates",
			"latitude":51.00366,
			"longitude":-114.12126
		},
		"address": {
			"@type": "PostalAddress",
			"addressLocality": "Calgary",
			"addressRegion": "Alberta",
			"postalCode": "T3E 6L1",
			"streetAddress": "400 – 4954 Richard Road SW",
			"addressCountry":"CA"
		}
	},
  "employee": {
    "@type": "OrganizationRole",
    "name": "Jefferey Cave",
    "startDate": "2009-01",
    "endDate": "2009-12",
    "roleName": ".Net Developer/Analyst"
  }
}
</script>


# [Pandell Technology](http://www.pandell.com/)

<iframe style="border: 0;" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2510.334172342904!2d-114.12930268354306!3d51.00997497955668!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x537171c5ab578cff%3A0xac3fcc92b7f6c08b!2sPandell!5e0!3m2!1sen!2sca!4v1537205845611" width="460" height="260" frameborder="0"><span data-mce-type="bookmark" style="display: inline-block; width: 0px; overflow: hidden; line-height: 0;" class="mce_SELRES_start">﻿</span></iframe>

## .Net Developer/Analyst (2009)

| :--- | ---: |
| [Calgary,AB](https://www.google.ca/maps/place/Calgary,+AB/@51.0127803,-114.3550282,10z/data=!3m1!4b1!4m5!3m4!1s0x537170039f843fd5:0x266d3bb1b652b63a!8m2!3d51.0486151!4d-114.0708459) | 2009-01 &rarr; 2009-12 |

* Continued development, support, and integration of acquired systems Joint-Venture accounting systems
* Responsible for conducting Customer Conversions (extract data from competitor systems into a JVNexus instance)

(see also Project: "Pandell Competitor Conversion")

<sub>C#, .NET, msSQL</sub>


