---
layout: default
title: Newpace Technology
---



<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Corporation",
	"name": "Newpace Technology",
	"url": "http://newpace.com/",
	//"logo":"nsha.jpeg",
	"location":{
		"@type":"Place",
		"geo":{
			"@type": "GeoCoordinates",
			"latitude":44.73244,
			"longitude":-63.66099
		}
	},
	"alternateName":"Sigmast Communication",
	"employee": {
		"@type": "OrganizationRole",
		"name": "Jefferey Cave",
		"startDate": "2015-12",
		"endDate": "2017-09-04"
	},
	"members":[
		{
			"@type": "Corporation",
			"name": "Neustar",
			"url": "https://numbering.neustar.biz/",
			"location":{
				"@type":"Place",
				"address":"21575 Ridgetop Cir, Sterling, VA 20166, USA",
				"geo":{
					"@type": "GeoCoordinates",
					"latitude":39.027599,
					"longitude":-77.4081567
				}
			},
			"employee": {
				"@type": "OrganizationRole",
				"name": "Jefferey Cave",
				"startDate": "2015-12",
				"endDate": "2017-08",
				"roleName":"Senior QA Engineer"
			}
		},
		{
			"@type": "Corporation",
			"name": "Samsung",
			"url": "https://www.samsung.com/ca/",
			"location":{
				"@type":"Place",
				"geo":{
					"@type": "GeoCoordinates",
					"latitude":37.60881,
					"longitude":126.91974
				}
			},
			"employee": {
				"@type": "OrganizationRole",
				"name": "Jefferey Cave",
				"startDate": "2016-12",
				"endDate": "2017-08",
				"roleName":"Senior QA Analyst"
			}
		}
	]
}
</script>

(now Sigmast)

[https://www.newpace.com/](https://www.newpace.com/)

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-63.665095567703254%2C44.730371014645954%2C-63.65687727928162%2C44.73452488255392&amp;layer=mapnik&amp;marker=44.732444175005604%2C-63.66098642349243" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=44.73244&amp;mlon=-63.66099#map=17/44.73245/-63.66099&amp;layers=N">View Larger Map</a></small>

## Senior QA Analyst

| :--- | :---: | ---: |
| [Samsung](http://www.samsung.com/ca/) | [Halifax, NS](https://www.google.ca/maps/place/NewPace+Technology+Development+Inc./@44.7324575,-63.6631306,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5989c6b3fee4ed:0x2f551911441f6e63!8m2!3d44.7324575!4d-63.6609366?hl=en) | 2016-12 &rarr; 2017-08 |

* Preparation of product for deployment with new brand association
* Requirements analysis against [GSMA Specification](https://www.gsma.com/futurenetworks/wp-content/uploads/2017/07/RCC.71_v1.0.pdf)
* Reconciliation against aged test plans

<sub>Perl, Python, Bash, SIPp, Test Planning, TestLink</sub>

## Senior QA Engineer

| :--- | :---: | ---: |
| Neustar | [Stirling, VA](https://www.google.ca/maps/place/Neustar,+Inc./@39.0303473,-77.4105878,15z/data=!4m5!3m4!1s0x0:0x9f8c49ff155531dd!8m2!3d39.027747!4d-77.40591) | 2015-12 &rarr; 2016-12 |

* Ongoing development and management of Neustar’s PortPS Suite
* Development of distributed Automated Testing system
* Explicit reporting system (consistent and unbiased results)

<sub>Java, TestNG, Selenium, GIT, Oracle, PouchDB, Javascript, HTML</sub>


