---
layout: default
title: Nova Scotia Community College
---

<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "CollegeOrUniversity",
	"name": "Nova Scotia Community College",
	"url": "https://www.nscc.ca/",
	//"logo":"nsha.jpeg",
	"location":{
		"@type": "Place",
		"name":"NSCC IT Campus",
		"geo": {
			"@type": "GeoCoordinates",
			"latitude": "44.73244",
			"longitude": "-63.66099"
		}
	},
	"alternateName":"NSCC",
	"parentOrganization":{
		"@type":"GovernmentOrganization",
		"name":"Government of Nova Scotia"
	},
	"employee": {
		"@type": "EmployeeRole",
		"roleName":"Faculty",
		"name": "Jefferey Cave",
		"startDate": "2017-09-05",
		"endDate": "2018-10-09",
		"baseSalary":73000,
		"salaryCurrency":"CAD"
	}
}
</script>

[https://www.nscc.ca/](https://www.nscc.ca/)

<iframe width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=-63.616665601730354%2C44.66850408359733%2C-63.61255645751954%2C44.67058327534106&amp;layer=mapnik&amp;marker=44.669543688792444%2C-63.61461102962494" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/?mlat=44.66954&amp;mlon=-63.61461#map=18/44.66954/-63.61461">View Larger Map</a></small>

## Faculty

| :--- | :---: | ---: |
| [IT and Creative Services](https://www.nscc.ca/) | [Halifax, NS](https://www.google.ca/maps/place/NSCC+-+Institute+of+Technology+Campus/@44.669196,-63.615631,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a21117093ee6d:0x875cd1d0d4665725!8m2!3d44.669196!4d-63.613437?hl=en) | 2017-09 &rarr; 2018-09 |

* Introductory Programming, Web Development, Project Management
* Student Counselling

<sub>Python, Bash, Node.js, CouchDB, PHP, D3, HTML</sub>

