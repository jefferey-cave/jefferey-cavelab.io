---
layout: default
title: Sunergon Information Services
---

<script type="application/ld+json">
{
	"@context": "http://schema.org",
	"@type": "Corporation",
	"name": "Sunergon Information Systems",
	"employee": {
		"@type": "OrganizationRole",
		"name": "Jefferey Cave",
		"startDate": "2001-11",
		"endDate": "2007-09",
		"roleName": "System Analyst and Developer"
	}
}
</script>

(2001 &mdash; 2007)

[http://www.sunergon.com/](http://www.sunergon.com/)

## System Analyst and Developer

| :--- | ---: |
| [Calgary,AB](https://www.google.ca/maps/place/Calgary+Co-op+Head+Office/@50.9756235,-114.0718088,17z/data=!4m12!1m6!3m5!1s0x53717121f80b2a97:0xd89a497fa5d8649!2sCalgary+Co-op+Head+Office!8m2!3d50.9756235!4d-114.0696148!3m4!1s0x53717121f80b2a97:0xd89a497fa5d8649!8m2!3d50.9756235!4d-114.0696148) | 2001-11 &rarr; 2007-08 |

* Designing custom business solutions for several fields: Environmental Reclamations, Oil & Gas Production Testing, Financial Management, Funeral Services, Telecommunications, Accounting
* Customer Relations, Requirements gathering, Expectation Management
* Implement Continuous Integration processes (TDD, Auto Build, Auto Testing)


(see also Projects:"Payroll Processing and Integration",”Previewdata”,”Sunergon Project Management”,”Fund Management System”,”RWDI – Virtual Wind”,”KBT Systems JIT Inventory”,”Remediation & Reclamations”,”Phone-in Cellular Recharge”,”Pre-paid Phone Cards”)

<sub>Process Design, Software Development Life Cycle, CI, TDD</sub>

## Developer

