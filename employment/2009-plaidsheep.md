---
layout: default
title: PlaidSheep Software
---

<script type="application/ld+json">
{
  "@context": "http://schema.org",
  "@type": "Corporation",
  "name": "PlaidSheep Software",
  "url": "https://www.plaidsheep.ca/",
  //"logo":"",
  "location":{
    "@type":"Place",
    "address":"830 West Dalhousie Road, Annapolis Royal, Nova Scotia, Canada"
  },
  "employee": {
    "@type": "OrganizationRole",
    "member": {
      "@type": "Person",
      "name": "Jefferey Cave"
    },
    "startDate": "2010-01",
    "endDate": "2014-12",
    "roleName": "Owner/Developer"
  },
  "customers":[
    {
      "@type": "Corporation",
      "name": "Service Nova Scotia and Municipal Affairs",
      "url": "https://novascotia.ca/dma/",
      "location":"Halifax, Nova Scotia, Canada",
      "parentOrganization":{
        "name": "Government of Nova Scotia"
      },
      "employee": {
        "@type": "OrganizationRole",
        "name": "Jefferey Cave",
        "startDate": "2013-08",
        "endDate": "2014-04",
        "roleName": "Software Developer"
      }
    },
    {
      "@type": "Corporation",
      "name": "Dalhousie Web Team",
      "url": "https://www.dal.ca/webteam.html",
      "location":"Halifax, Nova Scotia, Canada",
      "parentOrganization":{
        "name": "Dalhousie University"
      },
      "employee": {
        "@type": "OrganizationRole",
        "roleName": "Test Analyst",
        "startDate": "2013-01",
        "endDate": "2013-08",
        "name": "Jefferey Cave"
      }
    },
    {
      "@type": "Corporation",
      "name": "IWK Health Centre",
      "url": "http://www.iwk.nshealth.ca/",
      "location":"5850/5980 University Avenue Halifax, Nova Scotia, Canada, B3K 6R8",
      "employee": {
        "@type": "OrganizationRole",
        "roleName": "Clinical Applications Analyst",
        "startDate": "2010-07",
        "endDate": "2011-01",
        "name": "Jefferey Cave"
      }
    },
    {
      "@type": "Corporation",
      "name": "\"Just Say Yes\" Aboriginal Employment Program",
      "url": "https://www.dal.ca/webteam.html",
      "location":"Calgary, Alberta, Canada",
      "parentOrganization":{
        "name": "Jan Pat Managment Organization"
      },
      "employee": {
        "@type": "OrganizationRole",
        "roleName": "Test Analyst",
        "startDate": "2013-01",
        "endDate": "2013-08",
        "member": {
          "@type": "Person",
          "name": "Jefferey Cave"
        }
      }
    }
  ]
}
</script>

# PlaidSheep Software (2009-2013)

* Hosting and development of web based conference management system
  * Online Presence and marketing
  * Registration
  * Paper submission and Jurying
* Development and maintenance of online marketing material
* Maritime Heart Centre online presence and technical consulting

## Software Developer

| :--- | :---: | ---: |
| Government of Nova Scotia - SNSMR | [Halifax, NS](https://www.google.ca/maps/place/Victoria+General/@44.6377144,-63.5827977,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2230ac782cdd:0x57c4cd69b64a6416!8m2!3d44.6377144!4d-63.5806037) | 2013-08 &rarr; 2014-04 |

* Application and System development of Registry of Motor Vehicles
* Application support and customer relations
* Identification of system inefficiencies and design of corrective patterns 

<sub>Java, Subversion (SVN), J2EE, Oracle, plSQL, Normalization</sub>


## Web and Test Analyst

| :--- | :---: | ---: |
| Dalhousie University | [Halifax, NS](https://www.google.ca/maps/place/Victoria+General/@44.6377144,-63.5827977,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a2230ac782cdd:0x57c4cd69b64a6416!8m2!3d44.6377144!4d-63.5806037) | 2013-01 &rarr; 2013-08 |

* maintenance of the Google Search Appliance & Google Analytics
* Quality Assurance for Dalhousie’s Adobe AEM websites (http://dal.ca)

(see also Project:“Adobe CQ Mobile Website”)

<sub>Test Driven Development, Agile (Kanban/Scrum), TestNG, UnitTesting, Automated Regression Tests, Java, Selenium</sub>


## Clinical Applications Analyst 

| :--- | :---: | ---: |
| IWK Hospital | [Halifax, NS](https://www.google.ca/maps/place/IWK+Health+Centre/@44.6372597,-63.5864228,17z/data=!3m1!4b1!4m5!3m4!1s0x4b5a223a8faf12c1:0x6b34b6aa4c522a98!8m2!3d44.6372597!4d-63.5842288) | 2010-07 &rarr; 2011-01 |

* Responsible for pharmaceutical and dietary systems
* Initiated Automated Test systems for Meditech Upgrade

(see also Project:“IWK Meditech Upgrade”)

<sub>Meditech, Boston Workstation, Planning, Automated Testing, C#, NUnit</sub>


## Instructor

| :--- | :---: | ---: |
| "Just Say Yes" Aboriginal Employment Program | [Calgary,AB](https://www.google.ca/maps/place/Calgary,+AB/@51.0127803,-114.3550282,10z/data=!3m1!4b1!4m5!3m4!1s0x537170039f843fd5:0x266d3bb1b652b63a!8m2!3d51.0486151!4d-114.0708459) | 2010-04 &rarr; 2010-06 |

Just Say YES was a Work Experience program assisting at-risk Native Youth in 
skills upgrades and entry into the workforce

* Taught Introductory Computer usage
* Worked to create a curriculum that included the development of a simulated 
  start-up to focus on real-world application of skills

<sub>Lecturing, curriculum development</sub>


