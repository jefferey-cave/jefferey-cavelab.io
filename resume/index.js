'use strict';

(function(){

const state = {
	data:null
};


function Initialize(){
	LoadResume();
}

async function LoadResume(){
	let response = await fetch('resume.json');
	let json = await response.json();
	state.data = json;
	
	RenderResume();
}

async function RenderResume(){
	let elem = document.querySelector('#timeline');
	if(!elem.dataset.template){
		elem.dataset.template = elem.innerHTML;
	}
	let html = '';
	if(state.data){
		let data = JSON.clone(state.data);
		data.companies.forEach(c=>{
			if(!Array.isArray(c.employees)){
				c.employees = [c.employees];
			}
			
			let emp = (Array.isArray(c.members) ? c.members : [])
				.filter((m)=>{
					m = (m.employees || true) === true;
					return m;
				})
				.map((m)=>{
					return m.employees;
				})
				;
			c.employees = c.employees.concat(emp);
			c.employees.sort((a,b)=>a.startDate.localeCompare(b));
			
			c.startDate = c.employees.map(e=>e.startDate).sort()[0];
			c.endDate = c.employees.map(e=>e.endDate).sort().reverse()[0];
		});
		data.companies = data.companies
			.sort((a,b)=>{
				return a.startDate.localeCompare(b.startDate);
			})
			.reverse()
			;
		html = Mustache.render(elem.dataset.template, data);
	}
	elem.innerHTML = html;
}

window.addEventListener('load',Initialize);

})();

