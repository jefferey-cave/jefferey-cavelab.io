'use strict';

const caveUtils = {
	/**
	 * Read the settings from the form and convert them into a data
	 * object.
	 *
	 * Return: collection of key value pairs
	 */
	readOptions:function (){
		let form = document.querySelectorAll('#opts input');
		form = Array.from(form);
		let opt = {};
		for(let f in form){
			let field = form[f];
			let name = field.getAttribute('name');
			let value = field.value;
			opt[name] = parseInt(value,10);
		}

		return opt;

	},

	/**
	 * Disable all of my options
	 */
	enableOptions:function (enabled = true){
		if(enabled !== true && enabled !== false){
			console.warn('Nonsense value passed to enableOptions');
			return;
		}
		let form = document.querySelectorAll('#opts input');
		let disabled = !enabled;
		Array.from(form).forEach(function(field){
			field.disabled = disabled;
		});
	}
};


/**
 * Create a 'cloning' utility
 *
 * Based on JSON stringify and parse
 */
if(!JSON.clone){
	JSON.clone = (obj)=>{
		return JSON.parse(JSON.stringify(obj));
	};
}


/**
 * wait for the page to load before starting everything
 */
window.addEventListener('load',function(){
	let title = document.querySelector('body > header > h1');
	if(title){
		title.innerText = document.querySelector('head > title').innerText;
	}

	// set the favicon
	let favicon = document.querySelector('head > link[rel="shortcut icon"] ');
	if(!favicon){
		favicon = document.createElement('link');
		document.head.append(favicon);
		favicon.rel = 'shortcut icon';
	}
	favicon.href="https://s.gravatar.com/avatar/b0fbe9582d6e6d4e815554789f434c81?s=32";

	// make the menu button do something
	let menu = document.querySelector('button.showhide');
	menu.dataset.visibleState = JSON.stringify(true);
	menu.addEventListener('click',(e)=>{
		let isVisible = !JSON.parse(e.target.dataset.visibleState);
		e.target.dataset.visibleState = JSON.stringify(isVisible);
		Array.from(e.target.parentElement.childNodes).forEach((d)=>{
			if(d === e.target) return;
			if(! d.style) return;

			if(isVisible){
				d.style.display = d.dataset.oldDisplay;
			}
			else{
				d.dataset.oldDisplay = d.style.display;
				d.style.display = 'none';
			}
		})
	});
	let click = new MouseEvent('click', {
			view: window,
			bubbles: true,
			cancelable: true
		});
	menu.dispatchEvent(click);

});
