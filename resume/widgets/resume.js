'use strict';

// define the item component
Vue.component('resume', {
	template: '#resume-template',
	props: {
		submission: Submission,
		remover:{
			type:Function,
			default: null
		}
	},
	data: function () {
		return {};
	},

});

